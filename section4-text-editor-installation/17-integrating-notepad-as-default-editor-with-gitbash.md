Integrating notepad++ as default editor

##### 1. Creating alias for "notepad++"

'''
[WINDOWS OS]
Step1 - Go to user home dir(eg - C:\Users\chauhan_k)
Step2 - Create or modify ".bash_profile" file
Step3 - add alias for notepad++ as
      alias npp='notepad++.exe -multiInst -nosession'
(refer ./.bash_profile) in this section

Step4 - Restart git bas terminal and try "npp" then enter

refer - https://askubuntu.com/questions/969632/where-is-bash-profile-located-in-windows-subsystem-for-linux
'''

##### 2. Setting notepad++ as default editor for gitbash
```
git config --global core.editor "notepad++.exe -multiInst -nosession"
```