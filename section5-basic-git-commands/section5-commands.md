### Commands

#### 1 - Adding existing local project to git

```
1.1 git init
1.2 git init <folderName>

2. git remote add <repo-url-alias> <repo-url>(eg. git remote add origin <url>)
3. git status
4. git add .
5. git commit -m "inintial commit"
6. git push <repo-url-alias> master (eg. git push origin master)

```

#### 2 - Joining existing git project
```
We can do this by 2 ways
 - way 1 - cloning
 - way 2 - forking then cloning 
```
#### 2.1 - way 1 - Cloning a git repo to local

```
[cloning default branch]
1.1 git clone <repo-url>

[cloning specific branch]
1.2 git clone -b <branch/tag> <repo-url>

2 <make your changes>
3. git add .
4. git commit -m "<message>"
5. git pull origin <branchName>
6. git pull origin master - this is optional but a good practice
                          - its optional if we dont want our changes to merge in master branch
                          - else it's mandatory
7. git push origin master
```

#### 2.2 - way 2 - forking then cloning

```
 - fork using github ui - forking will copy any existing git prpjects to our own git hub repository.
 - once we fork then clone the project in our local  

```

#### TODO ADD DESCRIPTION

```
List folders and file of {baseDir}
1.1 git ls-files

List folders and file of {baseDir}/<folder-name>
1.2 git ls-files <folder-name>

```

#### 3 - Backing out changes
```
1. From Staging Area
[Unstage a file- Moving file from stage area to working dir]
1.1 git reset HEAD <file>

[Unstage a dir and all it subdir]
1.2 git reset HEAD <dir> 

[Unstage everything]
1.3 git reset HEAD .

2. From Working dir
2.1 git checkout -- <file>
2.2 git checkout -- <dir>
2.3 git checkout -- .

```

#### 4 - History(git log, git show)

```
4.1 - git log

[to get all options of git log]
1. git help log

2. git log
  - it will return recent commit in rverse chronological order 
     i.e. as commit timstamp DESC
3. git log --abbrev-commit
  - same as "git log", only diff here we will get short commit SHA id

4. git log --oneline --graph --decorate
  - it's same as "git log" but return 1 commit  in one line

5.  git log <COMMIT_SHA_ID1>...<COMMIT_SHA_ID2>
  - it return commit logs between the ranges of specified commit sha id
  - order of <COMMIT_SHA_ID1>...<COMMIT_SHA_ID2> is not matter
    we canspecify latest commit before or after ...

6. git log -- <file>
  - it returns commits hitory of specified file
  - eg. "git log -- README.md" 

4.2 - git show

1. git show <COMMIT_SHA_ID>
  - this return commit information
```