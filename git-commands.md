#### Git commands

#### 1. Basic commands
```bc
1.1 - git version 

1.2 - git status
      - tells the current branch
      - files in the different git states(i.e it tell th differnce b/w working dir and git repo(using .git dir ))

1.3 -  git add  - Moving file from working dir to stage area
[Stage Only specfic file]
1.3.1 - git add <file>
[Stage all the file of <dir> and sub dir and sub dir files]
1.3.2 - git add <dir>
[Stage everything]
1.3.3 - git add .

1.4 -  git commit  - Moving file from stage area to commit (i.e. .git folder)
    - git commit -m "commit message"
Note
 - we can combine git add and git commit command into one as
    - git commit -am "<message>"
      - Note - it will not add and commit untracked file(new file), 
               it will work for tracked files

1.4 -  git push  - Moving code from local repo to git hub repo
1.4.1 - git push <repo-url-alias> <branch>
```

#### 2. Git Configuration commands
```note
- Git requires some information before we can pull, push code from github.
  -- name
  -- email address
```

```qsc
To change git global config
2.1 - git config --global user.name "kaushlendra singh chauhan"
2.2 - git config --global user.email "kaushlendra277@gmail.com"

To get all global git config
2.3.1 - git config --global --list 
2.3.2 - git config --global -e

To get all git config (project-config + global config)
2.3.1 - git config --list 
2.3.2 - git config -e 

```

#### 3. Git clone commands

```cc
cloning default branch
3.1.1 - git clone <repo-url>

cloning non-default branch
3.1.2 - git clone -b <branch-name> <repo-url>
```